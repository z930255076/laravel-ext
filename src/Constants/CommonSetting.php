<?php

namespace EthanZ\LaravelExt\Constants;

class CommonSetting
{


    /*
    |--------------------------------------------------------------------------
    | 时效相关
    |--------------------------------------------------------------------------
    */
    // adminToken失效时间.
    public const ADMIN_TOKEN_TIME_OUT = 86400 * 7;

    // userToken失效时间.
    public const USER_TOKEN_TIME_OUT = 86400 * 30;


    /*
    |--------------------------------------------------------------------------
    | 数量相关
    |--------------------------------------------------------------------------
    */
    // 后台列表默认一页显示数量
    public const PAGE_SIZE = 15;


    /*
    |--------------------------------------------------------------------------
    | 登录平台
    |--------------------------------------------------------------------------
    */
    public const LOGIN_PLATFORM = ['web', 'app'];


    /*
    |--------------------------------------------------------------------------
    | 日志相关
    |--------------------------------------------------------------------------
    */
    public const LOG_MAX_DAY      = 30;

    public const LOG_FILE_SIZE    = 2097152;

    public const LOG_SQL_MAX_TIME = 1;

    public const LOG_RUN_MAX_TIME = 3;

    public static array $dateFormat = [
        'YEAR'   => 'Y',
        'MONTH'  => 'Y-m',
        'WEEK'   => 'Y年第W周',
        'DAY'    => 'Y-m-d',
        'HOUR'   => 'Y-m-d H',
        'MINUTE' => 'Y-m-d H:i',
        'SECOND' => 'Y-m-d H:i:s',
    ];
}
