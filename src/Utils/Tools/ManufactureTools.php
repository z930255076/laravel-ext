<?php

namespace EthanZ\LaravelExt\Utils\Tools;

use Illuminate\Support\Str;

/**
 * 其他工具类
 */
class ManufactureTools
{


    /**
     * 获取ip
     *
     * @param int $type 1普通格式 2int格式
     *
     * @return mixed|string
     */
    public static function getIp(int $type = 1): int|string
    {
        return $type === 1 ? request()?->getClientIp() : self::ipToInt(request()?->getClientIp());
    }


    /**
     * IP转int
     *
     * @param string $ip
     *
     * @return int
     */
    public static function ipToInt(string $ip): int
    {
        return sprintf('%u', ip2long($ip));
    }


    /**
     * token 生成
     *
     * @return string
     */
    public static function token(): string
    {
        $salt = Str::random(8);
        $str  = md5(uniqid(md5(microtime(true)), true));

        return sha1($str . $salt);
    }
}
