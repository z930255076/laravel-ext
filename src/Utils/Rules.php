<?php

namespace EthanZ\LaravelExt\Utils;

use EthanZ\LaravelExt\Constants\CommonSetting;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Validation\Validator;

/**
 * 自定义验证规则工具类
 */
class Rules extends FacadesValidator
{


    /**
     * 电话验证
     *
     * @param string    $attribute  要被验证的属性名称.
     * @param mixed     $value      属性的值.
     * @param array     $parameters 传入验证规则的参数数组.
     * @param Validator $validator  Validator 实列.
     *
     * @return int|false
     */
    public function phone(string $attribute, mixed $value, array $parameters, Validator $validator): int|false
    {
        return preg_match('/^(1)\d{10}$/', $value);
    }


    /**
     * 密码
     *
     * @param string    $attribute  要被验证的属性名称.
     * @param mixed     $value      属性的值.
     * @param array     $parameters 传入验证规则的参数数组.
     * @param Validator $validator  Validator 实列.
     *
     * @return int|false
     */
    public function pwd(string $attribute, mixed $value, array $parameters, Validator $validator): int|false
    {
        return preg_match('/(?=.*[a-z])(?=.*\d)(?=.*[#@!,~%^&*])[a-z\d#@!,~%^&*]{8,20}/', $value);
    }


    /**
     * 登录平台
     *
     * @param string    $attribute  要被验证的属性名称.
     * @param mixed     $value      属性的值.
     * @param array     $parameters 传入验证规则的参数数组.
     * @param Validator $validator  Validator 实列.
     *
     * @return bool
     */
    public function loginPlatform(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        return in_array($value, CommonSetting::LOGIN_PLATFORM, true);
    }
}
