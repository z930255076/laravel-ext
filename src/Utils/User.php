<?php

namespace EthanZ\LaravelExt\Utils;

use EthanZ\LaravelExt\Redis\Constants\UserRedisKey;
use EthanZ\LaravelExt\Redis\UserRedis;

/**
 * 账户工具类
 */
class User
{


    /**
     * 获取用户token数据
     *
     * @return array|null
     */
    public static function userTokenData(): ?array
    {
        // 获取token.
        $token = request()?->header('U-Token');
        if (!$token) {
            return [];
        }

        return UserRedis::query()->setKey(UserRedisKey::TOKEN_USER, $token)->get();
    }


    /**
     * 获取用户id
     *
     * @return int
     */
    public static function userId(): int
    {
        $tokenData = self::userTokenData();

        return $tokenData ? $tokenData['id'] : 0;
    }


    /**
     * 获取用户信息
     *
     * @return array
     */
    public static function userInfo(): array
    {
        $userId = self::userId();

        return UserRedis::query()->setKey(UserRedisKey::USER_ID_USER, $userId)->get();
    }
}
