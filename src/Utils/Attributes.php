<?php

namespace EthanZ\LaravelExt\Utils;

use Illuminate\Support\Facades\Validator;

/**
 * 验证错误的自定义属性工具类
 */
class Attributes extends Validator
{


    /**
     * 电话
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function phone(string $message, string $attribute, mixed $rule, array $parameters): mixed
    {
        return trans('lang.BAS_VLD_001');
    }


    /**
     * 密码
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function pwd(string $message, string $attribute, mixed $rule, array $parameters): mixed
    {
        return trans('lang.BAS_VLD_002');
    }


    /**
     * 登录平台
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function loginPlatform(string $message, string $attribute, mixed $rule, array $parameters): mixed
    {
        return trans('lang.BAS_VLD_003');
    }
}
