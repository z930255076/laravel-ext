<?php

namespace EthanZ\LaravelExt\Utils;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Str;

class TraceEntity
{


    /**
     * sql
     *
     * @var array
     */
    protected static array $sql = [];

    /**
     * rpc
     *
     * @var array
     */
    protected static array $rpc = [];

    /**
     * es
     *
     * @var array
     */
    protected static array $es = [];


    /**
     * 传入sql
     *
     * @param QueryExecuted $event
     */
    public static function setAnSql(QueryExecuted $event): void
    {
        $bindings = [];
        foreach ($event->bindings as $v) {
            $bindings[] = is_numeric($v) ? $v : "'$v'";
        }

        self::$sql[] = 'excute ' . $event->time . 'ms ' . Str::replaceArray('?', $bindings, $event->sql);
    }


    /**
     * 传入一个rpc
     *
     * @param array $backData
     */
    public static function setAnRpc(array $backData): void
    {
        self::$rpc[] = $backData;
    }


    /**
     * 传入一个es
     *
     * @param array $esData
     */
    public function setAnEs(array $esData): void
    {
        self::$es[] = $esData;
    }


    /**
     * 获取debug信息
     *
     * @return array
     */
    public static function getDebugData(): array
    {
        $data['sql'] = self::$sql;
        $data['rpc'] = self::$rpc;

//        $data['es']  = $this->es;

        return $data;
    }
}