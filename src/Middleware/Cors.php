<?php

namespace EthanZ\LaravelExt\Middleware;

use EthanZ\LaravelExt\Exceptions\NormalException;
use Closure;
use Illuminate\Http\Request;

/**
 * 用户登录验证
 */
class Cors
{


    /**
     * 响应前操作.
     *
     * @param Request $request 参数.
     * @param Closure $next    继续执行.
     *
     * @return mixed
     * @throws NormalException.
     */
    public function handle(Request $request, Closure $next): mixed
    {
        return $next($request)->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, U-Token, lang');
    }
}
