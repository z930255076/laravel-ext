<?php

namespace EthanZ\LaravelExt\Middleware;

use App\Http\Kernel as HttpKernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Routing\Router;

class Kernel extends HttpKernel
{


    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array<int, class-string|string>
     */
    protected $commonMiddleware = [
        ConvertEmptyStringsToNull::class,
        // sql|info日志中间件.
        SysLog::class,
        Cors::class,
    ];


    /**
     * The application's route middleware groups.
     *
     * @var array<string, array<int, class-string|string>>
     */
    protected $commonMiddlewareGroups = [
        'web' => [
        ],
        'api' => [
        ],
    ];


    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array<string, class-string|string>
     */
    protected $commonRouteMiddleware = [
        'user_login_check' => UserLoginCheck::class,
    ];


    public function __construct(Application $app, Router $router)
    {
        $this->app    = $app;
        $this->router = $router;

        // 全局中间件.
        $this->middleware = array_merge($this->commonMiddleware, $this->middleware);

        // 全局中间件组.
        foreach ($this->commonMiddlewareGroups as $k => $v) {
            $this->middlewareGroups[$k] = [...($this->middlewareGroups[$k] ?? []), ...$v];
        }

        // 路由中间件.
        $this->routeMiddleware = array_merge($this->commonRouteMiddleware, $this->routeMiddleware);
    }
}
