<?php

namespace EthanZ\LaravelExt\Server\InternalServer;

class FinanceUri
{

    // 资金
    public const FINANCE_CHART                   = '/finance/api/chart';

    public const FINANCE_PAGE                    = '/finance/api/page';

    public const FINANCE_ADD                     = '/finance/api/add';

    public const FINANCE_EDIT                    = '/finance/api/edit';

    public const FINANCE_DELETE                  = '/finance/api/delete';

    public const FINANCE_HYPE_INCOME_CALCULATION = '/finance/api/hype_income_calculation';


    // 资金类型
    public const FINANCE_TYPE_TREE   = '/finance/api/type/tree';

    public const FINANCE_TYPE_ADD    = '/finance/api/type/add';

    public const FINANCE_TYPE_EDIT   = '/finance/api/type/edit';

    public const FINANCE_TYPE_DELETE = '/finance/api/type/delete';


    // 明细相关
    public const FINANCE_INFO_PAGE   = '/finance/api/info/page';

    public const FINANCE_INFO_LIST   = '/finance/api/info/list';

    public const FINANCE_INFO_ADD    = '/finance/api/info/add';

    public const FINANCE_INFO_EDIT   = '/finance/api/info/edit';

    public const FINANCE_INFO_DELETE = '/finance/api/info/delete';


    // 明细相关
    public const FINANCE_DEBT_PAGE          = '/finance/api/debt/page';

    public const FINANCE_DEBT_ADD           = '/finance/api/debt/add';

    public const FINANCE_DEBT_DELETE        = '/finance/api/debt/delete';

    public const FINANCE_DEBT_RECORD_LIST   = '/finance/api/debt/record/list';

    public const FINANCE_DEBT_RECORD_ADD    = '/finance/api/debt/record/add';

    public const FINANCE_DEBT_RECORD_DELETE = '/finance/api/debt/record/delete';
}
