<?php

namespace EthanZ\LaravelExt\Server\InternalServer;

class NavUri
{

    // 导航
    public const NAV_LIST      = '/nav/api/list';

    public const NAV_TYPE_LIST = '/nav/api/type/list';

    // 待办
    public const NAV_TODO_FIND   = '/nav/api/todo/find';

    public const NAV_TODO_LIST   = '/nav/api/todo/list';

    public const NAV_TODO_ADD    = '/nav/api/todo/add';

    public const NAV_TODO_FINISH = '/nav/api/todo/finish';

}
