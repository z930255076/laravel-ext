<?php

namespace App\Requests;

use EthanZ\LaravelExt\Requests\BaseRequest;
use EthanZ\LaravelExt\Requests\Common\CommonRule;
use EthanZ\LaravelExt\Requests\Common\PageCommonRequest;
use EthanZ\LaravelExt\Exceptions\NormalException;

/**
 * 验证器
 */
class ExampleRequest extends BaseRequest
{

    use PageCommonRequest;


    // required验证必须有值: null,'',[],空可计数对象,文件为空
    // sometimes有传输该字段则验证后面的规则（没用）
    // unique重复验证
    // after:tomorrow明天之后；after:start_date开始时间之后。

    /**
     * 所有规则
     *
     * @var array
     */
    protected array $rules = [
        // 用户名重复验证(表、字段、获取排除值字段、排除字段).
        'nick_name'   => 'required|unique:users,nick_name,{id},id|' . CommonRule::ACCOUNT,
        'password'    => 'required|pwd',
        'platform'    => 'required|login_platform',
        'phone'       => 'phone',
        // 验证数据是否存在.
        'id'          => 'required|exists:users,id',
        // 数组id、name是否存在重复
        'foo.*.name' => 'distinct',
        'url'         => 'url',
        'json'         => 'json',
    ];


    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes(): array
    {
        return array_merge(
            $this->pageAttributes(),
            [
                'account'  => trans('lang.USR_LOG_006'),
                'password' => trans('lang.USR_LOG_007'),
                'platform' => trans('lang.USR_LOG_008'),
                // 错误消息位置
                'page.*.id' => '第 :position 张照片',
            ]
        );
    }


    /**
     * 场景验证规则
     *
     * @var array
     */
    protected array $scenes = [
        'login'  => ['account', 'password', 'platform'],
        'login1' => ['phone', 'nick_name', 'user', 'unique_name', 'url', 'page', 'page_size'],
    ];


    /**
     * 验证规则
     *
     * @return array
     * @throws NormalException
     */
    public function rules(): array
    {
        // 获取所有规则.
        $rules = array_merge($this->rules, $this->pageRules());
//        // 添加规则.
//        $this->addFieldRule($this->addFieldRule ?? [], $rules);
//        // 删除规则.
//        $this->delFieldRule($this->delFieldRule ?? [], $rules);
//        // 重置规则.
//        $this->resFieldRule($this->resFieldRule ?? [], $rules);

        return $this->getRule($this->scenes, $rules);
    }


    /**
     * 场景验证规则追加
     *
     * @var array
     */
    protected array $addFieldRule = [
        'hypeIncomeCalculation' => [
            'typeIds' => 'required',
        ],
    ];


    /**
     * 场景验证规则删除
     *
     * @var array
     */
    protected array $delFieldRule = [
        'page' => [
            'type'     => 'required',
            'lentBody' => 'required',
        ],
    ];


    /**
     * 场景验证规则删除
     *
     * @var array
     */
    protected array $resFieldRule = [
        'page' => [
            'type'     => 'required',
            'lentBody' => 'required',
        ],
    ];
}
