<?php

namespace EthanZ\LaravelExt\Requests\Common;

/**
 * 公共规则
 */
class CommonRule
{
    public const  ID          = 'digits_between:1,20';

    public const  TIME        = 'digits:10';

    public const  ACCOUNT     = 'alpha_num|between:6,20';

    public const  PRICE       = 'numeric';

    public const  DESCRIPTION = 'between:0,500';
}
