<?php

namespace EthanZ\LaravelExt\Requests\Common;

/**
 * 分页公共验证器
 */
trait PageCommonRequest
{


    /**
     * 规则
     *
     * @return array
     */
    public function pageRules(): array
    {
        return [
            'page'      => 'digits_between:1,20',
            'page_size' => 'integer|gte:1|max:100',
        ];
    }


    /**
     * 验证错误自定义属性
     *
     * @return array
     */
    public function pageAttributes(): array
    {
        return [
            'page'      => trans('lang.BAS_VLD_011'),
            'page_size' => trans('lang.BAS_VLD_012'),
        ];
    }
}
