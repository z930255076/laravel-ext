<?php

namespace EthanZ\LaravelExt\Exceptions;

use Exception;

class RpcException extends Exception
{

    public array  $errorData = [];

    public int    $needLog   = 1;

    public string $codeStr;

    public function __construct(string $codeStr = 'ERROR', string $msg = '', array $errData = [])
    {
        $this->codeStr   = $codeStr;
        $this->message   = $msg ?: trans('lang.' . $codeStr);
        $this->errorData = $errData;

        parent::__construct($this->message);
    }
}