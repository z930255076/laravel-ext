<?php

namespace EthanZ\LaravelExt\Exceptions;

use Throwable;
use Exception;

class BaseException extends Exception
{

    public array  $errorData = [];

    public int    $needLog   = 0;

    public string $codeStr;


    public function __construct(string $codeStr = 'ERROR', ?Throwable $previous = null, string $msg = '')
    {
        $this->codeStr = $codeStr;
        $this->message = $msg ?: trans('lang.' . $codeStr);
        if ($previous instanceof self || $previous instanceof RpcException) {
            $this->codeStr   = $previous->codeStr;
            $this->message   = $previous->message;
            $this->needLog   = $previous->needLog;
            $this->errorData = $previous->errorData;
        } elseif ($previous) {
            $this->errorData = [
                'file'  => $previous->getFile(),
                'line'  => $previous->getLine(),
                'msg'   => $previous->getMessage(),
                'trace' => $previous->getTrace(),
            ];
        }

        parent::__construct($this->message);
    }
}