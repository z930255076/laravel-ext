<?php

namespace EthanZ\LaravelExt\Exceptions;

class LogException extends BaseException
{
    public int $needLog = 1;
}
