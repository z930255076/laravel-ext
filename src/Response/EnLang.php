<?php

namespace EthanZ\LaravelExt\Response;

class EnLang
{
    protected static array $lang = [
        // 公共.
        'SUCCESS'     => '成功',
        'ERROR'       => '网络繁忙，请稍后再试',
        // url相关.
        'BAS_URL_001' => '路由错误',
        // 参数相关.
        'BAS_PAM_001' => '数据未找到',
        'BAS_PAM_002' => '参数错误',
//        'BAS_PAM_003' => '参数错误',
        // 频繁.
        'BAS_RAT_001' => '请求太频繁，请稍后再试',
        // 验证参数.
        'BAS_VLD_001' => '电话格式错误',
        'BAS_VLD_002' => '密码必须是数字、字母、特殊字符的组合，长度必须在 8 和 20 之间',
        'BAS_VLD_003' => '登录平台错误',
        'BAS_VLD_004' => '用户名已存在',
        'BAS_VLD_005' => '开始时间',
        'BAS_VLD_006' => '结束时间',
        'BAS_VLD_007' => '时间',
        'BAS_VLD_008' => '状态',
        'BAS_VLD_009' => '标题',
        'BAS_VLD_010' => '内容',
        'BAS_VLD_011' => '分页页码',
        'BAS_VLD_012' => '分页大小',
        // 数据相关.
        'BAS_DAT_001' => '未定义软删除字段',
        // 其他.
//        'BAS_OTH_001' => '服务出错了',


        // admin登录.
        'ADM_LOG_001' => '密码错误',
        'ADM_LOG_002' => '请先登录',
        'ADM_LOG_003' => '登录信息已过期',
        'ADM_LOG_004' => '登录信息变更，请重新登录',
        'ADM_LOG_005' => '异地登录',
        'ADM_LOG_006' => '账号',
        'ADM_LOG_007' => '密码',


        // user登录.
        'USR_LOG_001' => '密码错误',
        'USR_LOG_002' => '请先登录',
        'USR_LOG_003' => '登录信息已过期',
        'USR_LOG_004' => '登录信息变更，请重新登录',
        'USR_LOG_005' => '异地登录',
        'USR_LOG_006' => '账号',
        'USR_LOG_007' => '密码',
        'USR_LOG_008' => '平台',
        'USR_LOG_009' => '非法盗用',


        // 资金.
        'FNC_TYP_001' => '资金类型',
        'FNC_FNC_001' => '总资产',
        'FNC_FNC_002' => '收入',
        'FNC_FNC_003' => '支出',
        'FNC_FNC_004' => '月',
        'FNC_FNC_005' => '金额',
        'FNC_FIF_001' => '发生时间',
        'FNC_FIF_002' => '描述',
        'FNC_FIF_003' => '月份',
        'FNC_DDT_001' => '金额',
        'FNC_DDT_002' => '债务类型',
        'FNC_DDT_003' => '债务时间',
        'FNC_DDT_004' => '债务人',
        'FNC_DDT_005' => '描述',
        'FNC_DDT_006' => '债务状态',


        // 导航.
        'NAV_NAV_001' => '类型',
        'NAV_TOD_001' => '重要级别',

    ];

    public static function lang(): array
    {
        return self::$lang;
    }
}
