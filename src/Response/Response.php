<?php

namespace EthanZ\LaravelExt\Response;

use EthanZ\LaravelExt\Utils\TraceEntity;

/**
 * 响应类
 */
class Response
{


    /**
     * 响应操作.
     *
     * @param mixed|null $data
     * @param string     $code
     * @param string     $msg
     * @param array      $error
     *
     * @return object
     */
    public static function statusReturn(mixed $data = null, string $code = 'SUCCESS', string $msg = '', array $error = []): object
    {
        $returnData = [
            'code' => Code::$code[$code],
            'msg'  => $msg ?: trans('lang.' . $code),
            'data' => $data,
        ];

        // 获取链路信息.
        if (env('APP_ENV') !== 'pro' || (int)request('admin_debug') === env('ADMIN_DEBUG', 1024)) {
            $returnData['debug'] = [
                'runtime'  => microtime(true) - (constant('LARAVEL_START') ?? (float)($_SERVER['REQUEST_TIME_FLOAT'] ?? 0)),
                'memory'   => sprintf("%3.2f", memory_get_usage() / 1024 / 1024) . "M",
                'error'    => $error,
                'slow_log' => TraceEntity::getDebugData(),
            ];
        }

        return response($returnData, 200)
            ->header('Content-Type', 'application/json')
            ->header('Charset', 'utf-8');
    }
}
