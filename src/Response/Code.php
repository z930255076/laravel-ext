<?php

namespace EthanZ\LaravelExt\Response;

class Code
{
    public static array $code = [
        // 基础.
        'SUCCESS'     => 1,
        'ERROR'       => 0,
        // url相关.
        'BAS_URL_001' => 100100001,
        // 参数相关.
        'BAS_PAM_001' => 100101001,
        'BAS_PAM_002' => 100101002,
//        'BAS_PAM_003' => 100101003,
        // 频繁.
        'BAS_RAT_001' => 100102001,
        // 数据相关.
        'BAS_DAT_001' => 100103001,
        // 其他.
//        'BAS_OTH_001' => 100900001,


        // admin登录.
        'ADM_LOG_001' => 101100001,


        // user登录.
        'USR_LOG_001' => 102100001,
        'USR_LOG_002' => 102100002,
        'USR_LOG_003' => 102100003,
        'USR_LOG_004' => 102100004,
        'USR_LOG_005' => 102100005,
        'USR_LOG_009' => 102100009,
    ];
}
