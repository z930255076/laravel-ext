<?php

namespace EthanZ\LaravelExt\Redis\Constants;

class UserRedisKey
{


    // token.
    public const TOKEN_USER   = ['tku', 86400 * 30];

    public const USER_ID_USER = ['utu', 86400 * 30];

    // lang.
    public const TOKEN_LANG = ['uln', -1];
}
