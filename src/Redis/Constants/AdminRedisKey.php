<?php

namespace EthanZ\LaravelExt\Redis\Constants;

class AdminRedisKey
{


    // token.
    public const TOKEN_ADMIN    = ['tka', 86400 * 30];

    public const ADMIN_ID_ADMIN = ['ata', 86400 * 30];
}
