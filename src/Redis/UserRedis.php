<?php

namespace EthanZ\LaravelExt\Redis;

class UserRedis extends BaseRedis
{


    public function __construct()
    {
        parent::__construct();

        $this->appName = env('SIMPLE_USER');
    }
}
