<?php

namespace EthanZ\LaravelExt\Redis;

class AdminRedis extends BaseRedis
{


    public function __construct()
    {
        parent::__construct();

        $this->appName = env('SIMPLE_ADMIN');
    }
}
