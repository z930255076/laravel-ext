<?php

namespace EthanZ\LaravelExt\Listeners;

use EthanZ\LaravelExt\Utils\TraceEntity;
use Illuminate\Database\Events\QueryExecuted;

class SqlListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param QueryExecuted $event
     *
     * @return void
     */
    public function handle(QueryExecuted $event): void
    {
        // 链路.
        if (env('APP_ENV') !== 'pro' || (int)request('admin_debug') === env('ADMIN_DEBUG', 1024)) {
            TraceEntity::setAnSql($event);
        }
    }

}
