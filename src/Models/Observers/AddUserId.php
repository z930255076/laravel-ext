<?php

namespace EthanZ\LaravelExt\Models\Observers;

use Illuminate\Database\Eloquent\Model;
use EthanZ\LaravelExt\Utils\User;

/**
 * 自动添加用户ID
 *
 * @package EthanZ\LaravelExt\Models\Observers
 */
class AddUserId
{


    /**
     * 新增时
     *
     * @param Model $model
     */
    public function creating(Model $model): void
    {
        $userId = request('user_id');
        if ($userId) {
            $model->user_id = $userId;
        }
    }
}