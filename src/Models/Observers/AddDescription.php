<?php

namespace EthanZ\LaravelExt\Models\Observers;

use Illuminate\Database\Eloquent\Model;

/**
 * 自动添加描述
 *
 * @package EthanZ\LaravelExt\Models\Observers
 */
class AddDescription
{


    /**
     * 新增|修改时
     *
     * @param Model $model
     */
    public function saving(Model $model): void
    {
        $description = request('description');
        if ($description) {
            $model->description = $description;
        }
    }
}