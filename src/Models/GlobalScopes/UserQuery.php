<?php

namespace EthanZ\LaravelExt\Models\GlobalScopes;

use EthanZ\LaravelExt\Utils\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * 自动查询当前客户数据
 *
 * @package EthanZ\LaravelExt\Models\GlobalScopes
 */
class UserQuery implements Scope
{

    /**
     * 查询当前用户
     *
     * @param Builder $builder
     * @param Model   $model
     */
    public function apply(Builder $builder, Model $model): void
    {
        $userId = request('user_id');
        if ($userId) {
            $builder->whereIn('user_id', [0, $userId]);
        }
    }
}