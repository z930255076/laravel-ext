<?php

namespace EthanZ\LaravelExt\Models\GlobalScopes;

use EthanZ\LaravelExt\Utils\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * 软删除
 *
 * @package EthanZ\LaravelExt\Models\GlobalScopes
 */
class SoftDelete implements Scope
{

    /**
     * 查询当前用户
     *
     * @param Builder $builder
     * @param Model   $model
     */
    public function apply(Builder $builder, Model $model): void
    {
        $deletedAt = $model::DELETED_AT;
        // 自定义软删除过滤.
        if ($deletedAt) {
            $deletedAt = $model->getTable() . '.' . $deletedAt;
            $builder->where($deletedAt, 0);
        }
    }
}