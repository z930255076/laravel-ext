<?php

namespace EthanZ\LaravelExt\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Base extends Model
{


    /**
     * 定义时间格式（时间戳）
     *
     * @var string
     */
    protected $dateFormat = 'U';


    /**
     * 设置删除时间字段
     *
     * @var string
     */
    public const DELETED_AT = 'deleted_at';


    /**
     * 设置可操作字段
     *
     * @var array
     */
    public const OPERATION_FIELDS = [];


    /**
     * 不可批量赋值的属性.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * 是否修改时间字段
     *
     * @var string
     */
//    public $timestamps = false;


    /**
     * 设置返回时间为时间戳（$dateFormat新增和修改时间还是date）
     *
     * @param DateTimeInterface $date 时间.
     *
     * @return int
     */
    protected function serializeDate(DateTimeInterface $date): int
    {
        return $date->format('U');
    }
}
