<?php

namespace EthanZ\LaravelExt\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

/**
 * 自定义验证规则类
 */
class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        // 电话.
        {
            Validator::extend('phone', 'EthanZ\LaravelExt\Utils\Rules@phone');
            Validator::replacer('phone', 'EthanZ\LaravelExt\Utils\Attributes@phone');
        }

        // 密码.
        {
            Validator::extend('pwd', 'EthanZ\LaravelExt\Utils\Rules@pwd');
            Validator::replacer('pwd', 'EthanZ\LaravelExt\Utils\Attributes@pwd');
        }

        // 登录平台.
        {
            Validator::extend('login_platform', 'EthanZ\LaravelExt\Utils\Rules@loginPlatform');
            Validator::replacer('login_platform', 'EthanZ\LaravelExt\Utils\Attributes@loginPlatform');
        }
    }
}
