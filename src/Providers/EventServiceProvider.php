<?php

namespace EthanZ\LaravelExt\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use EthanZ\LaravelExt\Listeners\SqlListener;
use Illuminate\Database\Events\QueryExecuted;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        QueryExecuted::class => [
            SqlListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
