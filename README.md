# Ethan后端微服务公共包
本公共包仅用于laravel框架，请开发同学先熟悉laravel相关文档

## 一、项目结构
~~~bash
├── Constants #常量管理
├── Exceptions #异常服务相关
├── Listeners #监听
├── Log #日志
├── Middleware #中间件
├── Models #模型初始化
├── Providers #服务提供
├── Redis #redis初始化
├── Requests #验证初始化
├── Response #返回
├── Server #服务初始化
└── Utils # 工具集
~~~

## 二、安装
`` composer require ethan-z/laravel-ext ``


## 三、新建业务错误枚举类（每个抛错或日志都必须唯一）
    | 服务（三位）.模块（三位）.code（3位）
    | APP_ID（三位）.模块id（三位）.code（三位）
    | 中文可重复定义，每个报错对应一个code，方便定位
    | 如：ORD_OUT_001 = [200100001, '订单出库失败'];

## 四、文件修改
### 1、修改bootstrap/app.php 如下内容：
```php
$app->singleton(
Illuminate\Contracts\Http\Kernel::class,
//    App\Http\Kernel::class
EthanZ\LaravelExt\Middleware\Kernel::class
);
$app->singleton(
Illuminate\Contracts\Debug\ExceptionHandler::class,
//    App\Exceptions\Handler::class
EthanZ\LaravelExt\Exceptions\Handler::class
);
```

### 2、修改lang/lang.php 如下内容：
```php
use EthanZ\LaravelExt\Response\Lang;
$lang = Lang::lang();
return $lang;
```

### 3、修改config/app.php 如下内容：
```php
'locale' => app()->runningInConsole() ? 'en' : request()->header('lang'),
'providers' => [
    // 自定义验证规则类.
    \EthanZ\LaravelExt\Providers\ValidatorServiceProvider::class,
    // 公共事件.
    \EthanZ\LaravelExt\Providers\EventServiceProvider::class,
],
```

### 4、修改database.php 如下内容：
```php
'redis' => [
    'client' => env('REDIS_CLIENT', 'predis'),
    'options' => [
        'cluster' => env('REDIS_CLUSTER', 'redis'),
    ],
    'default' => [
        'url' => env('REDIS_URL'),
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'username' => env('REDIS_USERNAME'),
        'password' => env('REDIS_PASSWORD'),
        'port' => env('REDIS_PORT', '6379'),
        'database' => env('REDIS_DB', '0'),
    ],
    'cache' => [
        'url' => env('REDIS_URL'),
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'username' => env('REDIS_USERNAME'),
        'password' => env('REDIS_PASSWORD'),
        'port' => env('REDIS_PORT', '6379'),
        'database' => env('REDIS_CACHE_DB', '1'),
    ],
],
```

## 五、链路追踪
在env配置APP_ENV != prod则自动开启链路追踪 或 配置ADMIN_DEBUG在传参时传入对应的值


## 六、异常处理
使用示例1：
```php
throw new NormalException('USR_LOG_001');
```

使用示例2：
```php
throw new LogException('USR_LOG_001', $e, trans('lang.USR_LOG_001'));
```

## 七、服务
使用示例：
```php
Service::request()
->setPath(FinanceUri::FINANCE_List)
->setParams(['typeIds'=>[1]])
->get();
```

## 八、数据校验
### 1、创建验证
a) `必须继承BaseRequest`

b) `添加相应方法的场景`

b) `添加验证规则`：
像id、time等相同的验证最好使用BaseRequest的通用验证
公共验证规则单独写，如：PageCommonRequest

c) `自定义通用规则`：
在Utils-》Rules.php、Attributes.php进行新增验证，在Providers-》ValidatorServiceProvider.php进行引入验证规则

## 九、redis使用
### 1、每个服务必须在Constants下新建文件，并按照 public const INCR_NO = ['tku', 60]形式申明
### 2、每个服务必须在Redis下新建文件，并继承BaseRedis
### 3、使用示例
```php
UserRedis::query()
->setKey(UserRedisKey::TOKEN_USER, $token)
->get();
```

## 十、工具类
在Utils-》Tools.php进行新增


## 十一、多语言
前端heard中传入lang:zh,en


## 十二、模型生成注释
拉取第三方库：`composer require barryvdh/laravel-ide-helper --dev`
生成模型注释：`php artisan ide-helper:models "App\Models\Type"`


## 十三、自定义软删除
软删除示例(1.删除 0.恢复)：
```php
FinanceModel::query()
->where('id', 1)
->softDelete();

$userData->softDelete();
```

忽略软删除(模型关联上也可使用)：
```php
->withoutGlobalScope('delete');
```

查询软删除数据：
```php
->softDeleteData();
```

当前模型不使用：
```php
public const DELETED_AT = '';
```


## 十三、模型使用
```php
use Page, SoftDelete, OperationFields, GetSetColumns;

// OperationFields批量操作：查询当前客户数据，新增自动新增客户id、备注、创建时间、更新时间
public const OPERATION_FIELDS = [
    'user_id',
    'description',
];
batchInsert([...]);

// GetSetColumns数据降级
->getSetColumns(
    ['type.title' => 'type_title', 'top_type.title' => 'top_title'], // 降级字段转换
    ['type', 'topType'], // 降级后清除字段
    'get'
);
```

